
void* create_string_list(int n);
void free_string_list(void* pv, int n);
void insert_string(void* l, int n, char* s);
void inspect_string_list(void* lv, int n);
void free_string_list(void* pv, int n);
int fork_exec (int fcgi_fd, void* args, void* env);

